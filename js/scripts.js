$(document).ready(function() {
    // adding textillate effects on text
    $(".tlt").textillate({
        in : { effect: "fadeInLeft", sync: true },
        out : { effect: "fadeOutLeft", sync: true },
        loop: true
    });

    // setting tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // init carousel interval
    $("#itCarousel").carousel({interval: 3000});

    // setting modal buttons
    $("#viewCourseraModalBtn").click(function() {
        $("#coursera_modal").modal();
    });

    $("#viewCswpModalBtn").click(function() {
        $("#cswp_modal").modal();
    });

    $("#viewOsstmmModalBtn").click(function() {
        $("#osstmm_modal").modal();
    });
});